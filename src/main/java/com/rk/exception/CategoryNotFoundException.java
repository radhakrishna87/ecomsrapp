package com.rk.exception;

public class CategoryNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CategoryNotFoundException() {
	}
	
	public CategoryNotFoundException( String message) {
		super(message);
	}
}
