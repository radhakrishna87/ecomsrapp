package com.rk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rk.entity.Category;
import com.rk.repo.CategoryRepository;

@Service
public class CategoryServiceImpl implements ICategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@Override
	public Long saveCategory(Category category) {
		return categoryRepository.save(category).getId();
	}

	@Override
	public void updateCategory(Category category) {
		
		categoryRepository.save(category);
	}

	@Override
	public void deleteCategory(Long id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public Category getOneCategory(Long id) {

		return categoryRepository.getOne(id);
	}

	@Override
	public List<Category> getAllCategorys() {

		return categoryRepository.findAll();
	}

}
