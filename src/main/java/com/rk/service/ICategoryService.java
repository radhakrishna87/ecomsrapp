package com.rk.service;

import java.util.List;

import com.rk.entity.Category;

public interface ICategoryService {
	
	
	Long saveCategory(Category category);
	
	void updateCategory(Category category);
	
	void deleteCategory(Long id);
	
	Category getOneCategory(Long id);
	
	List<Category> getAllCategorys();

}
