package com.rk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rk.entity.Category;
import com.rk.service.ICategoryService;

@Controller
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private ICategoryService service;
	
	@GetMapping("/register")
	public String registerCategory(Model model) {
		model.addAttribute("category", new Category());
		return "CategoryRegister";
	}
	
	//save the category
	@PostMapping("/save")
	public String saeCategory( @ModelAttribute Category category, Model model) {
		
		Long id = service.saveCategory(category);
		model.addAttribute("message", "category created with the id:" +id);
		model.addAttribute("category", new Category());
		
		return "CategoryRegister";
		
	}
	
	public String getAllCategotys(Model model, @RequestParam(value ="message", required = false) String message) {
		List<Category> list = service.getAllCategorys();
		
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		
		return "CategoryData";
	}
	
	public String deleteCategory(@RequestParam Long id, RedirectAttributes attributes) {
	
		service.deleteCategory(id);
		attributes.addAttribute("message", "Category deleted with id:" +id);
		
		return "redirect:all";
		
	}
	
	@GetMapping("/edit")
	public String editCategory( @RequestParam Long id, Model model) {
		String page;
		
			Category ob = service.getOneCategory(id);
			model.addAttribute("category", ob);
			
			page = "CategoryEdit";
			
		return page;
	}
	
	public String updateCategory( @ModelAttribute Category category, RedirectAttributes attributes) {
		
		service.updateCategory(category);
		attributes.addAttribute("message", "Category Updated");
		return "redirect:all";
		
	}
	
}
